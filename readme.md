# PHP iCalendar file generator
Ported and modified to my needs from [this gist](https://gist.github.com/jakebellacera/635416)

## Basic usage

```php

require('ics.php');
$ics = new ics([
    'summary' => 'My Event',
    'dtstart' => 'now',
    'dtend' => 'now + 30 minutes'
]);
$ics->toIcsFile();

//or

require('ics.php');
$ics = new ics();
$ics->set([
  'summary' => 'My Event',
  'dtstart' => 'now',
  'dtend' => 'now + 30 minutes'
]);
$ics->toIcsFile();

//or

require('ics.php');
$ics = new ics();
$ics->set([
  'summary' => 'My Event',
  'dtstart' => 'now',
  'dtend' => 'now'
]);
$ics->setAllDay();
$ics->toGoogleCalendar();

```

### Available properties

* **description** - string description of the event.
* **dtend** - date/time stamp designating the end of the event. You can use either a `DateTime` object or a [PHP datetime format string](http://php.net/manual/en/datetime.formats.php) (e.g. "now + 1 hour").
* **dtstart** - date/time stamp designating the start of the event. You can use either a `DateTime` object or a [PHP datetime format string](http://php.net/manual/en/datetime.formats.php) (e.g. "now + 1 hour").
* **location** - string address or description of the location of the event.
* **summary** - string short summary of the event - usually used as the title.
* **url** - string url to attach to the the event. Make sure to add the protocol (`http://` or `https://`).

### Example
You can view the live example [here](http://www.upadrian.com/php-ics/example)

### License
Distributed under the MIT License. For more information, read the file LICENSE or peruse the license [online](https://gitlab.com/upadrian/php-ics/blob/master/LICENSE).

### Credits
* [Jake Bellacera](https://github.com/jakebellacera), original author
* [Alejandro Moreno](https://gitlab.com/amoreno1), contributor

