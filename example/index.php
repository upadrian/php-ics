<?php
require("../ics.php");
//check the form input and decide if we proccess or not.
if (proccessForm()) {
  $options = formatFields($_POST);
  $ics = new ics($options);
  if ($_POST['allday']) {

    $ics->setAllDay();
    $ics->set($options);
  }
  switch ($options['action']) {
    case 'btnMakeIcs':
      header("content-type:text/plain");
      echo $ics->toIcsString();
      die();
      break;
    case 'btnDownloadIcs':
      $ics->toIcsFile($options['summary']);
      die();
      break;
    case 'btnGoogleCalendar':
      $ics->toGoogleCalendar();
      die();
      break;
    case 'btnGoogleCalendarView':
      echo $ics->buildGoogleCalendarUrl();
      die();
      break;
  }
}
function proccessForm() {
  $rvars = [
    "action",
    "summary",
    "dtstart",
    "dtend",
    "location",
    "url",
    "description"
  ];
  foreach ($rvars as $var) {
    if (!isset($_POST[$var]) || !is_scalar($_POST[$var])) {
      return false;
    }
  }
  $ovars = [
    "summary",
    "dtstart",
    "dtend"
  ];
  foreach ($ovars as $var) {
    if ($_POST[$var] == '') {
      return false;
    }
  }
  if (!isset($_POST['allday'])) {
    $_POST['allday'] = 0;
  }
  if (!checkValidDate($_POST['dtstart']) || !checkValidDate($_POST['dtend'])) {
    return false;
  }
  if (!in_array($_POST['action'], ['btnMakeIcs', 'btnDownloadIcs', 'btnGoogleCalendar', 'btnGoogleCalendarView'])) {
    return false;
  }
  return true;
}
function formatFields($vars) {
  $dtstart = DateTime::createFromFormat("Y-m-d H:i", $vars['dtstart']);
  $vars['dtstart'] = $dtstart->format("Ymd\THis\Z");
  $dtend = DateTime::createFromFormat("Y-m-d H:i", $vars['dtend']);
  $vars['dtend'] = $dtend->format("Ymd\THis\Z");
  return $vars;
}
function checkValidDate($date) {
  $dt = DateTime::createFromFormat("Y-m-d H:i", $date);
  return $dt !== false && !array_sum($dt->getLastErrors());
}

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Example file for php-ics</title>
  <!-- styles not related and not required for ics.php -->
  <link rel="stylesheet" href="https://gitcdn.link/repo/Chalarangelo/mini.css/master/dist/mini-dark.min.css"/>
  <link rel="stylesheet" href="air-datepicker/css/datepicker.min.css"/>
  <link rel="stylesheet" href="example.css"/>
</head>
<body>
<h1>Example file for php-ics<br/>
  <small>Generates .ics file or create Google Calendar Event. <a href="https://gitlab.com/upadrian/php-ics" target="_blank" title="View project on Gitlab">View
      on Gitlab</a></small>
</h1>
<form id="formExample" action="" method="post" target="_blank">
  <input type="hidden" name="action" value="btnMakeIcs"/>
  <fieldset>
    <legend>Fields for the calendar</legend>
    <label>* Title of the event</label>
    <input type="text" name="summary" placeholder="Adrian birthday" required/>
    <label>* Start date of the event</label>
    <input type="text" name="dtstart" placeholder="2017-11-29 18:00" readonly required/>
    <label>* End date of the event</label>
    <input type="text" name="dtend" placeholder="2017-11-29 23:30" readonly required/>
    <div class="input-group">
      <input type="checkbox" name="allday" id="allday" value="1"/>
      <label for="allday"> All day</label>
    </div>
    <label>Location of the event</label>
    <input type="text" name="location" placeholder="Poughkeepsie, NY 12602-0666"/>
    <label>URL</label>
    <input type="text" name="url" placeholder="http://www.upadrian.com"/>
    <label>Description</label>
    <textarea name="description" placeholder="You are all invited to my birthday."></textarea>
  </fieldset>
  <fieldset class="actions">
    <legend>Actions:</legend>
    <input type="button" id="btnMakeIcs" value="Generate and show ics string"/>
    <input type="button" id="btnDownloadIcs" value="Download ics file"/>
    <input type="button" id="btnGoogleCalendar" value="Insert into google calendar"/>
    <input type="button" id="btnGoogleCalendarView" value="View url to insert into google calendar"/>
  </fieldset>
</form>
<!-- scripts not related and not required for ics.php -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
<script src="air-datepicker/js/datepicker.min.js"></script>
<script src="air-datepicker/js/i18n/datepicker.en.js"></script>
<script src="example.js"></script>
</body>
</html>

