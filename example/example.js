$(document).ready(function() {
  $("input[name=dtstart],input[name=dtend]").datepicker({
    language  : 'en',
    timepicker: true,
    timeFormat: 'hh:ii',
    dateFormat: 'yyyy-mm-dd'
  });
  $(document).delegate(".actions input[type=button]", "click", function() {
    var $form     = $("#formExample"),
        $btn      = $(this);
    if(
      $("input[name=summary]").val() === ''
      || $("input[name=dtstart]").val() === ''
      || $("input[name=dtend]").val() === ''
    ) {
      return;
    }
    $("input[name=action]").val($btn.attr("id"));
    $form.submit();
  });
});