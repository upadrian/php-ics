#Explanation about the available parameters on google calendar event.

##Anchor address:
```
http://www.google.com/calendar/event?
```

This is the base of the address before the parameters below.

##Parameters
```
action:
    action=TEMPLATE
    A default required parameter.
src:
    Example: src=default%40gmail.com
    Format: src=text
    This is not covered by Google help but is an optional parameter
    in order to add an event to a shared calendar rather than a user's default.
text:
    Example: text=Garden%20Waste%20Collection
    Format: text=text
    This is a required parameter giving the event title.
dates:
    Example: dates=20090621T063000Z/20090621T080000Z
           (i.e. an event on 21 June 2009 from 7.30am to 9.0am
            British Summer Time (=GMT+1)).
    Format: dates=YYYYMMDDToHHMMSSZ/YYYYMMDDToHHMMSSZ
           This required parameter gives the start and end dates and times
           (in GMT) for the event.
details:
    Example: details=Lorem%20ipsum%20dolor%20sit%20amet
    Specify the description of the event (event body text)
location:
    Example: location=Home
    Format: location=text
    The obvious location field.
trp:
    Example: trp=false
    Format: trp=true/false
    Show event as busy (true) or available (false)
sprop:
    Example: sprop=http%3A%2F%2Fwww.me.org
    Example: sprop=name:Home%20Page
    Format: sprop=website and/or sprop=name:website_name
add:
    Example: add=default%40gmail.com
    Format:  add=guest email addresses
```

##Notes
* All parameters values are url-encoded
* The dates can also be given without times - eg dates=20090621/20090621. Google Calendar will interpret this as an all-day event.

##References and thanks to:
* [Google old reference on waybackmachine](https://web.archive.org/web/20120313011336/http://www.google.com/googlecalendar/event_publisher_guide.html)
* [Ashley on blogspot](http://useroffline.blogspot.com.uy/2009/06/making-google-calendar-link.html)
* [This answer on stackoverflow](https://stackoverflow.com/a/23495015/2448530)

