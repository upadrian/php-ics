<?php

/**
 * Class ics
 * Ported from this gist: https://gist.github.com/jakebellacera/635416
 * @version 1.0.0
 * @author https://gitlab.com/upadrian | https://github.com/jakebellacera
 * @link https://gitlab.com/upadrian/php-ics
 * @link https://gist.github.com/jakebellacera/635416
 */
class ics {
  /**
   * Date format for the ics calendar (date with hour)
   */
  const DT_FORMAT = 'Ymd\THis\Z';
  /**
   * Date format for the ics calendar (date without hour)
   */
  const DT_FORMAT_ALLDAY = 'Ymd';
  /**
   * Collection of properties for this calendar
   * @var array
   */
  protected $properties = [];
  /**
   * @var array Available properties for the calendar
   * url, description, location, and summary: string
   * dtend and dtstart: date string for DateTime php class in GMT
   *
   */
  private $availableProperties = [
    'description',
    'dtend',
    'dtstart',
    'location',
    'summary',
    'url'
  ];
  /**
   * @var bool Set if the event is all day event
   */
  private $allDay = false;

  /**
   * ics constructor.
   * @param $props array|string As in $this->availableProperties
   */
  public function __construct($props) {
    $this->set($props);
  }

  /**
   * Unset all day event
   */
  public function unsetAllDay() {
    $this->allDay = false;
  }

  /**
   * Set all day event
   */
  public function setAllDay() {
    $this->allDay = true;
  }

  /**
   * @param $key string A key from $this->availableProperties
   * @param bool $val string|date
   */
  public function set($key, $val = false) {
    if (is_array($key)) {
      foreach ($key as $k => $v) {
        $this->set($k, $v);
      }
    } else {
      if (in_array($key, $this->availableProperties)) {
        $this->properties[$key] = $this->sanitize_val($val, $key);
      }
    }
  }

  /**
   * Generate string with .ics format calendar
   * @return string
   */
  public function toIcsString() {
    $rows = $this->buildIcs();
    return implode("\r\n", $rows);
  }

  /**
   * Force download .ics calendar
   * @param $name string Filename for download the .ics calendar
   */
  public function toIcsFile($name) {
    $string = $this->toIcsString();
    header('Content-Type:text/Calendar');
    header('Content-Disposition: attachment; filename=' . urlencode($name) . '.ics');
    header('Pragma: no-cache');
    header('Expires: 0');
    echo $this->toIcsString();
    die();
  }

  /**
   * Generate Google Calendar Url for make the event
   * @return string
   */
  public function buildGoogleCalendarUrl() {
    $url = 'https://www.google.com/calendar/event?';
    $url .= http_build_query(
      [
        'action'   => 'TEMPLATE',
        'text'     => $this->properties['summary'],
        'dates'    => $this->properties['dtstart'] . '/' . $this->properties['dtend'],
        'details'  => $this->properties['description'],
        'location' => $this->properties['location']
      ]
    );
    return $url;
  }

  /**
   * Redirect browser to google calendar and generate the event
   */
  public function toGoogleCalendar() {
    $url = $this->buildGoogleCalendarUrl();
    header("location:{$url}");
    die();
  }

  /**
   * Build ICS calendar
   * @return array
   */
  private function buildIcs() {
    // Build ICS properties - add header
    $ics_props = [
      'BEGIN:VCALENDAR',
      'VERSION:2.0',
      'PRODID:-//hacksw/handcal//NONSGML v1.0//EN',
      'CALSCALE:GREGORIAN',
      'BEGIN:VEVENT'
    ];
    // Build ICS properties - add header
    $props = [];
    foreach ($this->properties as $k => $v) {
      $props[strtoupper($k . ($k === 'url' ? ';VALUE=URI' : ''))] = $v;
    }
    // Set some default values
    $props['DTSTAMP'] = $this->format_timestamp('now');
    $props['UID'] = uniqid();
    // Append properties
    foreach ($props as $k => $v) {
      $ics_props[] = "$k:$v";
    }
    // Build ICS properties - add footer
    $ics_props[] = 'END:VEVENT';
    $ics_props[] = 'END:VCALENDAR';
    return $ics_props;
  }

  private function sanitize_val($val, $key = false) {
    /*
    * todo:
    * Lines of text SHOULD NOT be longer than 75 octets, excluding the line break.
    * Long content lines SHOULD be split into a multiple line representations using a line "folding" technique.
    * That is, a long line can be split between any two characters by inserting a CRLF immediately followed by
    * a single linear white-space character (i.e., SPACE or HTAB). Any sequence of CRLF followed immediately
    * by a single linear white-space character is ignored (i.e., removed) when processing the content type.
    */
    switch ($key) {
      case 'description':
        break;
      case 'dtend':
      case 'dtstamp':
      case 'dtstart':
        $val = $this->format_timestamp($val);
        break;
      default:
        $val = $this->escape_string($val);
    }
    return $val;
  }

  private function format_timestamp($timestamp) {
    $dt = new DateTime($timestamp);
    return $dt->format($this->allDay ? self::DT_FORMAT_ALLDAY : self::DT_FORMAT);
  }

  private function escape_string($str) {
    return preg_replace('/([\,;])/', '\\\$1', $str);
  }
}

